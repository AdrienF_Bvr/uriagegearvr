﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BVRCrosshair : MonoBehaviour {
    public enum CrosshairStatus  {
        hidden,
        started,
        done
    }
    public Image timer;
    public Image crosshair;
    private CrosshairStatus status = CrosshairStatus.hidden;
    private float totalTime = float.MaxValue;
    private float startingTime = float.MaxValue;
    private float elapsed_time = 0;
	// Use this for initialization
	void Start () {
        //timer.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	    if (status == CrosshairStatus.started)
        {
            elapsed_time = Time.time - startingTime;
            timer.fillAmount = elapsed_time / totalTime;
            if(elapsed_time > totalTime)
            {
                cancel();
            }
        }
	}
    public void cancel()
    {
        Debug.Log("Crosshair cancelling after "+ elapsed_time + " " + elapsed_time/totalTime);
        elapsed_time = 0;
        status = CrosshairStatus.hidden;
        timer.gameObject.SetActive(false);
        totalTime = float.MaxValue;
        timer.fillAmount = 0;
    }
    public void activateTimer(float totalTime)
    {
        Debug.Log("Crosshair activating");
        if (status > CrosshairStatus.hidden)
        {
            cancel();
        }

        status = CrosshairStatus.started;
        startingTime = Time.time;
        this.totalTime = totalTime;
        timer.gameObject.SetActive(true);
    }
}
