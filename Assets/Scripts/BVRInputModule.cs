﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class BVRInputModule : PointerInputModule {

    public bool handle_click = true;
    public bool handle_gaze = true;
    public float GazeTimeinSeconds = 2f;
    public RaycastResult currentRaycast;
    private PointerEventData pointerEventData;
    private GameObject currentLookAtHandler;
    private float currentLookAtHandlerClickTime;

	

    public override void Process()
    {
       
        HandleLook();
       
        HandleSelection();
        
    }

    void HandleLook()
    {
        //emulate a fake pointer and link raycast to it
        if(pointerEventData == null)
        {
            pointerEventData = new PointerEventData(eventSystem);
        }
        //fake pointer
        pointerEventData.position = new Vector2(Screen.width / 2, Screen.height / 2);
        pointerEventData.delta = Vector2.zero;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        
        eventSystem.RaycastAll(pointerEventData, raycastResults);
        currentRaycast = pointerEventData.pointerCurrentRaycast = FindFirstRaycast(raycastResults);
        ProcessMove(pointerEventData);
    }

    void HandleSelection()
    {
        if(pointerEventData.pointerEnter != null)
        {
            
            GameObject handler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(pointerEventData.pointerEnter);
            //switch handler if user changed target
            if(currentLookAtHandler != handler)
            {
                currentLookAtHandler = handler;
                currentLookAtHandlerClickTime = Time.realtimeSinceStartup + GazeTimeinSeconds;
                
                if (handle_gaze)
                 {
                    BVRCrosshair timer = GetComponent<BVRCrosshair>();
                    if(handler!= null) {
                        //pointer entered a new object
                        Debug.Log("Input module, activate timer");
                        timer.activateTimer(GazeTimeinSeconds);
                    }
                    else
                    {
                        //pointer exited an object
                        Debug.Log("Input module, cancel stuff");
                        timer.cancel();
                    }
                    
                 }

        
            }

            if(currentLookAtHandler!= null && ((handle_click && Input.GetButtonDown("Submit")) || (handle_gaze && Time.realtimeSinceStartup > currentLookAtHandlerClickTime)))
            {
                //trigger the selection
                EventSystem.current.SetSelectedGameObject(currentLookAtHandler);
                //trigger the click
                Debug.Log("Input module click");
                ExecuteEvents.ExecuteHierarchy(currentLookAtHandler, pointerEventData, ExecuteEvents.pointerClickHandler);
                currentLookAtHandlerClickTime = float.MaxValue; //reset the time to a huge value
                ExecuteEvents.ExecuteHierarchy(EventSystem.current.currentSelectedGameObject, pointerEventData, ExecuteEvents.deselectHandler); /// deselect
            }

        }
        else
        {
           
            currentLookAtHandler = null;
        }

    }


}
